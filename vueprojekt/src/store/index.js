import Vue from "vue"
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        trainerIn: [],
        teilnehmerIn: [],
        gruppen: [],
        raeume: [],
        gruppe: '',
        faecher: [],
        unterrichtstage: [],
        files: []
    },
    mutations: {
        getAlleTrainerInnen(state, trainerIn) {
            state.trainerIn = trainerIn
        },
        getAlleTeilnehmerInnen(state, teilnehmerIn) {
            state.teilnehmerIn = teilnehmerIn
        },
        getAlleGruppen(state, gruppen) {
            state.gruppen = gruppen
        }, getAlleRaeume(state, raeume) {
            state.raeume = raeume
        },
        getAlleFaecher(state, faecher) {
            state.faecher = faecher
        },
        getAlleUnterrichtstage(state, unterrichtstage) {
            state.unterrichtstage = unterrichtstage
        },
        getGruppeById(state, gruppe) {
            state.gruppe = gruppe
        },
        getAllFiles(state, files) {
            state.files = files;
        }
    },
    actions: {
        async uploadFile(context, file) {
            let formData = new FormData();
            formData.append("file", file);
            // let response = await Vue.axios.get("http://localhost:8080/teilnehmerInnen", formData);
            let response = await Vue.axios.post("http://localhost:8080/files/upload", formData);
            console.log(response);
            context.dispatch('getAllFiles')
        },

        async getAllFiles(context) {
            const response = await Vue.axios.get("http://localhost:8080/files");
            context.commit('getAllFiles', response.data)
        },


        async getAlleTrainnerInnen(context) {
            const response = await Vue.axios.get("http://127.0.0.1:8080/trainerInnen");
            context.commit('getAlleTrainerInnen', response.data)
        },
        async getAlleTeilnehmerInnen(context) {
            const response = await Vue.axios.get('http://localhost:8080/teilnehmerInnen');
            context.commit('getAlleTeilnehmerInnen', response.data)
        },
        async getAlleGruppen(context) {
            const response = await Vue.axios.get('http://localhost:8080/gruppe');
            context.commit('getAlleGruppen', response.data)
            console.log(response)
        },
        async getGruppeById(context, gruppen_ID) {
            await Vue.axios.get('http://localhost:8080/gruppe/' + gruppen_ID);
            context.commit('getGruppeById')
            //console.log(response)
        },
        async getAlleRaeume(context) {
            const response = await Vue.axios.get('http://localhost:8080/raum');
            context.commit('getAlleRaeume', response.data)
            console.log(response)
        },
        async getAlleFaecher(context) {
            const response = await Vue.axios.get('http://localhost:8080/fach');
            context.commit('getAlleFaecher', response.data)
            console.log(response)
        },
        async getAlleUnterrichtstage(context) {
            const response = await Vue.axios.get('http://localhost:8080/unterrichtstag');
            context.commit('getAlleUnterrichtstage', response.data)
            console.log(response)
        },


        async deleteTrainerIn(context, id) {
            await Vue.axios.delete('http://localhost:8080/trainerInnen/' + id);
            context.dispatch('getAlleTrainnerInnen')
        },
        async deleteTeilnehmerIn(context, id) {
            await Vue.axios.delete('http://localhost:8080/teilnehmerInnen/' + id);
            context.dispatch('getAlleTeilnehmerInnen')
        },
        async deleteGruppe(context, gruppen_ID) {
            await Vue.axios.delete('http://localhost:8080/gruppe/' + gruppen_ID);
            context.dispatch('getAlleGruppen')
        },
        async deleteRaum(context, raum_id) {
            await Vue.axios.delete('http://localhost:8080/raum/' + raum_id);
            context.dispatch('getAlleRaeume')
        },
        async deleteFach(context, fach_id) {
            await Vue.axios.delete('http://localhost:8080/fach/' + fach_id);
            context.dispatch('getAlleFaecher')
        },
        async deleteUnterrichtstag(context, unterrichtId) {
            await Vue.axios.delete('http://localhost:8080/unterrichtstag/' + unterrichtId);
            context.dispatch('getAlleUnterrichtstage')
        },



        async postTrainerIn(context, trainerInnen) {
            await Vue.axios.post('http://localhost:8080/trainerInnen/', trainerInnen)
            context.dispatch('getAlleTrainnerInnen')
        },
        async postTeilnehmerIn(context, teilnehmerIn) {
            await Vue.axios.post('http://localhost:8080/teilnehmerInnen/', teilnehmerIn)
            context.dispatch('getAlleTeilnehmerInnen')
        },
        async postGruppe(context, gruppe) {
            await Vue.axios.post('http://localhost:8080/gruppe', gruppe)
            context.dispatch('getAlleGruppen')
        },
        async postRaum(context, raum) {
            await Vue.axios.post('http://localhost:8080/raum', raum)
            context.dispatch('getAlleRaeume')
        },
        async postFach(context, fach) {
            await Vue.axios.post('http://localhost:8080/fach', fach)
            context.dispatch('getAlleFaecher')
        },
        async postUnterrichtstag(context, unterrichtstag) {
            await Vue.axios.post('http://localhost:8080/unterrichtstag', unterrichtstag)
            context.dispatch('getAlleUnterrichtstage')
        },


        async putTrainerIn(context, editTrainerInnen) {
            await Vue.axios.put('http://localhost:8080/trainerInnen/' + editTrainerInnen.id, editTrainerInnen)
            context.dispatch('getAlleTrainnerInnen')
        },
        async putTeilnehmerIn(context, editTeilnehmerInnen) {
            await Vue.axios.put('http://localhost:8080/teilnehmerInnen/' + editTeilnehmerInnen.id, editTeilnehmerInnen)
            context.dispatch('getAlleTeilnehmerInnen')
        },
        async putGruppe(context, editGruppen) {
            console.log(editGruppen)
            await Vue.axios.put('http://localhost:8080/gruppe/' + editGruppen.gruppen_ID, editGruppen)
            context.dispatch('getAlleGruppen')
        },
        async putRaum(context, editRaeume) {
            console.log(editRaeume)
            await Vue.axios.put('http://localhost:8080/raum/' + editRaeume.raum_id, editRaeume)
            context.dispatch('getAlleRaeume')
        },
        async putFach(context, editFaecher) {
            await Vue.axios.put('http://localhost:8080/fach/' + editFaecher.fach_id, editFaecher)
            context.dispatch('getAlleFaecher')
        },
        async putUnterrichtstag(context, editUnterrichtstage) {
            await Vue.axios.put('http://localhost:8080/unterrichtstag/' + editUnterrichtstage.unterrichtId, editUnterrichtstage)
            context.dispatch('getAlleUnterrichtstage')
        }

    }
})
