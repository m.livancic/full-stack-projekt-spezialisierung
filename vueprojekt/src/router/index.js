import Vue from 'vue'
import VueRouter from 'vue-router'
import HomePage from '../components/HomePage.vue'
import TrainerIn from '../components/TrainerIn.vue'
import TeilnehmerInnen from '../components/TeilnehmerInnen.vue'
import GruppeVue from '../components/GruppeVue.vue'
import RaumVue from '../components/RaumVue.vue'
import FachVue from '../components/FachVue.vue'
import UnterrichtsTag from '../components/UnterrichtsTag.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: HomePage
    },
    {
        path: '/trainerIn',
        name: 'TrainerIn',
        component: TrainerIn,
        // component: () => import('../components/TrainerIn.vue')
    },
    {
        path: '/teilnehmerIn',
        name: 'TeilnehmerInnen',
        component: TeilnehmerInnen
    },
    {
        path: '/gruppen',
        name: 'Gruppen',
        component: GruppeVue
    },
    {
        path: '/raum',
        name: 'Raum',
        component: RaumVue
    },
    {
        path: '/fach',
        name: 'Fach',
        component: FachVue
    },
    {
        path: '/unterrichtstag',
        name: 'Unterrichtstag',
        component: UnterrichtsTag
    }
]

const router = new VueRouter({
    routes
})

export default router