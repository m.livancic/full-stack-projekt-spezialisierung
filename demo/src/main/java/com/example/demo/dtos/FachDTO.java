package com.example.demo.dtos;

import com.example.demo.enums.FachEnums;

import java.sql.Clob;

public class FachDTO {
    private String description;
    private FachEnums fachEnums;
    private String bild64;

    public FachDTO(String description, FachEnums fachEnums, String bild64) {
        this.description = description;
        this.fachEnums = fachEnums;
        this.bild64 = bild64;

    }

    // getter und setter


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FachEnums getFachEnums() {
        return fachEnums;
    }

    public void setFachEnums(FachEnums fachEnums) {
        this.fachEnums = fachEnums;
    }

    public String getBild64() {
        return bild64;
    }

    public void setBild64(String bild64) {
        this.bild64 = bild64;
    }
}
