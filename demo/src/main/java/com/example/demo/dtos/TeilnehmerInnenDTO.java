package com.example.demo.dtos;

public class TeilnehmerInnenDTO {
    private String name;
    private String eMail;
    private String adresse;
    private String telnum;
    private String startdatum;
    private String enddatum;

    private int gruppeId;

    public TeilnehmerInnenDTO(String name, String eMail, String adresse, String telnum, String startdatum, String enddatum, int gruppeId) {
        this.name = name;
        this.eMail = eMail;
        this.adresse = adresse;
        this.telnum = telnum;
        this.startdatum = startdatum;
        this.enddatum = enddatum;
        this.gruppeId = gruppeId;
    }

    // getter und setter


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTelnum() {
        return telnum;
    }

    public void setTelnum(String telnum) {
        this.telnum = telnum;
    }

    public String getStartdatum() {
        return startdatum;
    }

    public void setStartdatum(String startdatum) {
        this.startdatum = startdatum;
    }

    public String getEnddatum() {
        return enddatum;
    }

    public void setEnddatum(String enddatum) {
        this.enddatum = enddatum;
    }

    public int getGruppeId() {
        return gruppeId;
    }

    public void setGruppeId(int gruppeId) {
        this.gruppeId = gruppeId;
    }
}
