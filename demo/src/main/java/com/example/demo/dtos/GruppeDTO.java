package com.example.demo.dtos;

import com.example.demo.enums.GruppenEnums;

public class GruppeDTO {
    private int nummer;
    private GruppenEnums gruppenEnums;
    private Boolean qualifying;
    private int raumId;

    public GruppeDTO(int nummer, GruppenEnums gruppenEnums, Boolean qualifying, int raumId) {
        this.nummer = nummer;
        this.gruppenEnums = gruppenEnums;
        this.qualifying = qualifying;
        this.raumId = raumId;
    }

    // getter und setter


    public int getNummer() {
        return nummer;
    }

    public void setNummer(int nummer) {
        this.nummer = nummer;
    }

    public GruppenEnums getGruppenEnums() {
        return gruppenEnums;
    }

    public void setGruppenEnums(GruppenEnums gruppenEnums) {
        this.gruppenEnums = gruppenEnums;
    }

    public int getRaumId() {
        return raumId;
    }

    public void setRaumId(int raumId) {
        this.raumId = raumId;
    }

    public Boolean getQualifying() {
        return qualifying;
    }

    public void setQualifying(Boolean qualifying) {
        this.qualifying = qualifying;
    }
}
