package com.example.demo.controller;

import com.example.demo.dtos.UnterrichtsTagDTO;
import com.example.demo.model.UnterrichtsTag;
import com.example.demo.service.UnterrichtsTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/unterrichtstag")
public class UnterrichtsTagController {

    @Autowired
    private UnterrichtsTagService unterrichtsTagService;

    public UnterrichtsTagController(UnterrichtsTagService unterrichtsTagService) {
        this.unterrichtsTagService = unterrichtsTagService;
    }

    @CrossOrigin
    @GetMapping
    public List<UnterrichtsTag> getUnterrichtsTag() {
        return unterrichtsTagService.getUnterrichtsTag();
    }

    @CrossOrigin
    @PostMapping
    public void unterrichtstagNeu (@RequestBody UnterrichtsTagDTO unterrichtsTagDTO) {

        unterrichtsTagService.setUnterrichtsTag(unterrichtsTagDTO.getTageEnums(), unterrichtsTagDTO.getTrainerId(), unterrichtsTagDTO.getFachId(), unterrichtsTagDTO.getGruppeId());

    }

    @CrossOrigin
    @DeleteMapping(path = "{unterrichtstagId}")
    public void unterrichtstagLoeschen(@PathVariable("unterrichtstagId") Integer unterrichtstagId) {
        unterrichtsTagService.unterrichtstagLoeschen(unterrichtstagId);
    }



    @CrossOrigin
    @PutMapping(path = "{unterrichtstagId}")
    public void unterrichtstagUpdate(
            @RequestBody UnterrichtsTagDTO unterrichtsTagDTO, @PathVariable("unterrichtstagId") Integer unterrichtstagId) {
        unterrichtsTagService.unterrichtstagUpdate(unterrichtsTagDTO, unterrichtstagId);
    }
}
