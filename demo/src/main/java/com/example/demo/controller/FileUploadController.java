package com.example.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Controller
public class FileUploadController {

    @GetMapping
    public String hello() {
        return "uploader";
    }

    @PostMapping("/upload111")
    public ResponseEntity<?> handleFileUpload (@RequestParam("file")MultipartFile file) throws IOException {
        String filename = file.getOriginalFilename();

        try {
            file.transferTo(new File("C:\\Users\\Marko Livancic\\Downloads\\upload\\" + filename));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok("File uploaded successfully.");
    }


}

