package com.example.demo.model;



import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class TeilnehmerInnen extends Person {

    @Column
    private String startdatum;

    @Column
    private String enddatum;

    @ManyToOne
    @JoinColumn(name = "gruppe_id")
    @JsonBackReference(value = "gruppe")
    private Gruppe gruppe;

    public TeilnehmerInnen(){}

    public TeilnehmerInnen(String name, String eMail, String adresse, String telnum, String startdatum, String enddatum, Gruppe gruppe){
        super(name, eMail, adresse, telnum);

        this.startdatum = startdatum;
        this.enddatum = enddatum;
        this.gruppe = gruppe;
    }

    public TeilnehmerInnen(String name, String eMail, String adresse, String telnum, String startdatum, String enddatum){
        super(name, eMail, adresse, telnum);

        this.startdatum = startdatum;
        this.enddatum = enddatum;
    }

    public TeilnehmerInnen(String name, String eMail) {
    }

    public TeilnehmerInnen(String name, String adresse, String eMail, String telefonnummer) {
    }




    //getter und setter


    public String getStartdatum() {
        return startdatum;
    }

    public void setStartdatum(String startdatum) {
        this.startdatum = startdatum;
    }

    public String getEnddatum() {
        return enddatum;
    }

    public void setEnddatum(String enddatum) {
        this.enddatum = enddatum;
    }

    public Gruppe getGruppe() {
        return gruppe;
    }

    public void setGruppe(Gruppe gruppe) {
        this.gruppe = gruppe;
    }


}
