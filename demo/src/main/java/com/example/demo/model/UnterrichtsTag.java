package com.example.demo.model;

import com.example.demo.enums.TageEnums;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Value;

import javax.persistence.*;

@Entity
@Table
public class UnterrichtsTag {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int unterrichtId;

    @Enumerated(EnumType.STRING)
    private TageEnums tageEnums;


    @ManyToOne
    @JsonManagedReference(value="fachunt")
    @JoinColumn(name = "fachId")
    private Fach fach;

    @ManyToOne
    @JsonManagedReference(value="gruunt")
    @JoinColumn(name = "gruppeId")
    private Gruppe gruppe;

    @ManyToOne
    @JsonManagedReference(value="traunt")
    @JoinColumn(name = "trainerInnenId")
    private TrainerInnen trainerInnen;

    public UnterrichtsTag(){}

    public UnterrichtsTag(TageEnums tageEnums, Fach fach, Gruppe gruppe, TrainerInnen trainerInnen) {
        this.tageEnums = tageEnums;
        this.fach = fach;
        this.gruppe = gruppe;
        this.trainerInnen = trainerInnen;
    }




    //getter und setter


    public TageEnums getTageEnums() {
        return tageEnums;
    }

    public void setTageEnums(TageEnums tageEnums) {
        this.tageEnums = tageEnums;
    }

    public Fach getFach() {
        return fach;
    }

    public void setFach(Fach fach) {
        this.fach = fach;
    }

    public Gruppe getGruppe() {
        return gruppe;
    }

    public void setGruppe(Gruppe gruppe) {
        this.gruppe = gruppe;
    }

    public TrainerInnen getTrainerInnen() {
        return trainerInnen;
    }

    public void setTrainerInnen(TrainerInnen trainerInnen) {
        this.trainerInnen = trainerInnen;
    }

    public int getUnterrichtId() {
        return unterrichtId;
    }

    public void setUnterrichtId(int unterrichtId) {
        this.unterrichtId = unterrichtId;
    }
}

//TODO CamelCase