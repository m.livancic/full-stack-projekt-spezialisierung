package com.example.demo.model;

import com.example.demo.enums.FachEnums;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.sql.Clob;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Fach {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int fach_id;

    @Column
    private String description;

    @Enumerated(EnumType.STRING)
    private FachEnums fachEnums;

    @Column(length=400000)
    private String bild64;

    @OneToMany(mappedBy = "fach")
    @JsonIgnore
    private Set<TrainerInnen> trainerInnenSet = new HashSet<>();

    @OneToMany(mappedBy = "fach")
    @JsonBackReference(value="fachunt")
//    @JsonIgnore
    private Set<UnterrichtsTag> unterrichtsTagSet = new HashSet<>();

    public Fach(){}

    public Fach(String description, FachEnums fachEnums) {
        this.description = description;
        this.fachEnums = fachEnums;
    }

    public Fach(String description, FachEnums fachEnums, String bild64){
        this.description = description;
        this.fachEnums = fachEnums;
        this.bild64 = bild64;
    }

    //getter und setter


    public int getFach_id() {
        return fach_id;
    }

    public void setFach_id(int fach_id) {
        this.fach_id = fach_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FachEnums getFachEnums() {
        return fachEnums;
    }

    public void setFachEnums(FachEnums fachEnums) {
        this.fachEnums = fachEnums;
    }

    public Set<TrainerInnen> getTrainerInnenSet() {
        return trainerInnenSet;
    }

    public void setTrainerInnenSet(Set<TrainerInnen> trainerInnenSet) {
        this.trainerInnenSet = trainerInnenSet;
    }

    public Set<UnterrichtsTag> getUnterrichtsTagSet() {
        return unterrichtsTagSet;
    }

    public void setUnterichtsTag(UnterrichtsTag unterrichtsTag) {
        this.unterrichtsTagSet.add(unterrichtsTag);
    }

    public void setTrainnerInnen(TrainerInnen trainerInnen) {
        this.trainerInnenSet.add(trainerInnen);
    }

    public void unterrichtLoeschen(UnterrichtsTag unterrichtsTag) {
        unterrichtsTagSet.remove(unterrichtsTag);
    }

    public void fachLoeschen(TrainerInnen trainerInnen) {
        trainerInnenSet.remove(trainerInnen);
    }

    public String getBild64() {
        return bild64;
    }

    public void setBild64(String bild64) {
        this.bild64 = bild64;
    }
}
