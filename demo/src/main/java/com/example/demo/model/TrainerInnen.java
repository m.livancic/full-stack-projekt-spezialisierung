package com.example.demo.model;

import com.example.demo.enums.AnstellungEnums;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.awt.geom.RoundRectangle2D;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "trainerIn")
public class TrainerInnen extends Person {

    @Enumerated(EnumType.STRING)
    private AnstellungEnums anstellungsart;

    @Column(nullable = false)
    private float bruttoGehalt;


    @ManyToOne
    @JoinColumn(name = "fachId")
    private Fach fach;


    @OneToMany(mappedBy = "trainerInnen")
    @JsonBackReference(value="traunt")
    private Set<UnterrichtsTag> unterrichtsTagSet = new HashSet<>();


    public TrainerInnen(){}

    public TrainerInnen(String name, String eMail, String adresse, String telnum, AnstellungEnums anstellungsart, float bruttoGehalt, Fach fach){
        super(name, eMail, adresse, telnum);

        this.anstellungsart = anstellungsart;
        this.bruttoGehalt = bruttoGehalt;
        this.fach = fach;
    }

    public TrainerInnen(String name, String eMail, String adresse, String telnum, AnstellungEnums anstellungsart, float bruttoGehalt){
        super(name, eMail, adresse, telnum);

        this.anstellungsart = anstellungsart;
        this.bruttoGehalt = bruttoGehalt;
    }

    public TrainerInnen(String name, String eMail) {
    }

    public TrainerInnen(String name, String eMail, String adresse, String telnum) {
    }


    //getter und setter


    public AnstellungEnums getAnstellungsart() {
        return anstellungsart;
    }

    public void setAnstellungsart(AnstellungEnums anstellungsart) {
        this.anstellungsart = anstellungsart;
    }

    public float getBruttoGehalt() {
        return bruttoGehalt;
    }

    public void setBruttoGehalt(float bruttoGehalt) {
        this.bruttoGehalt = bruttoGehalt;
    }

    public Fach getFach() {
        return fach;
    }

    public void setFach(Fach fach) {
        this.fach = fach;
    }

    public void setUnterichtsTag(UnterrichtsTag unterrichtsTag) {
        this.unterrichtsTagSet.add(unterrichtsTag);
    }

    public void unterrichtLoeschen(UnterrichtsTag unterrichtsTag) {
        unterrichtsTagSet.remove(unterrichtsTag);
    }

}





