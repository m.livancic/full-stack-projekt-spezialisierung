package com.example.demo.repositories;

import com.example.demo.model.TeilnehmerInnen;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeilnehmerInnenCRUDRepository extends CrudRepository <TeilnehmerInnen, Integer> {


}
