package com.example.demo.repositories;

import com.example.demo.model.Raum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RaumCRUDRepository extends CrudRepository<Raum, Integer> {
}
