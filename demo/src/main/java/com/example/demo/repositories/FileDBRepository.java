package com.example.demo.repositories;

import com.example.demo.model.DataModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileDBRepository extends JpaRepository<DataModel, String> {
}
