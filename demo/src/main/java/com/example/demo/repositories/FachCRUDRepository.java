package com.example.demo.repositories;

import com.example.demo.model.Fach;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FachCRUDRepository extends CrudRepository<Fach, Integer> {
}
