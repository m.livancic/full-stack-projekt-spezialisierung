package com.example.demo.repositories;

import com.example.demo.model.TrainerInnen;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrainerInnenCRUDRepository extends CrudRepository <TrainerInnen, Integer> {


}
