package com.example.demo.repositories;

import com.example.demo.model.Gruppe;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GruppeCRUDRepository extends CrudRepository<Gruppe, Integer> {
}
