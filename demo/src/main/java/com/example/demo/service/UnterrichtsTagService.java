package com.example.demo.service;

import com.example.demo.dtos.UnterrichtsTagDTO;
import com.example.demo.enums.TageEnums;
import com.example.demo.model.*;
import com.example.demo.repositories.FachCRUDRepository;
import com.example.demo.repositories.GruppeCRUDRepository;
import com.example.demo.repositories.TrainerInnenCRUDRepository;
import com.example.demo.repositories.UnterrichtsTagCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnterrichtsTagService {

    @Autowired
    private UnterrichtsTagCRUDRepository unterrichtsTagCRUDRepository;

    @Autowired
    private TrainerInnenCRUDRepository trainerInnenCRUDRepository;

    @Autowired
    private FachCRUDRepository fachCRUDRepository;

    @Autowired
    private GruppeCRUDRepository gruppeCRUDRepository;

    public List<UnterrichtsTag> getUnterrichtsTag() {
        return (List<UnterrichtsTag>) unterrichtsTagCRUDRepository.findAll();
    }

    public void setUnterrichtsTag(TageEnums tageEnums, Integer trainnerinnenId, Integer fachId, Integer gruppeId) {
        TrainerInnen trainerInnen = trainerInnenCRUDRepository.findById(trainnerinnenId).get();
        Fach fach = fachCRUDRepository.findById(fachId).get();
        Gruppe gruppe = gruppeCRUDRepository.findById(gruppeId).get();

        UnterrichtsTag unterrichtsTag = new UnterrichtsTag(tageEnums, fach, gruppe, trainerInnen);

        trainerInnen.setUnterichtsTag(unterrichtsTag);
        gruppe.setUnterichtsTag(unterrichtsTag);
        fach.setUnterichtsTag(unterrichtsTag);


        trainerInnenCRUDRepository.save(trainerInnen);
        fachCRUDRepository.save(fach);
        gruppeCRUDRepository.save(gruppe);
        unterrichtsTagCRUDRepository.save(unterrichtsTag);
    }

    public void neuerUnterrichtsTag(UnterrichtsTag unterrichtsTag) {
        unterrichtsTagCRUDRepository.save(unterrichtsTag);
    }

    public void unterrichtstagLoeschen(Integer unterrichtstagId) {
        boolean exists = unterrichtsTagCRUDRepository.existsById(unterrichtstagId);
        if (!exists) {
            throw new IllegalStateException("Unterrichtstag mit id " + unterrichtstagId + " existiert nicht");
        }
        unterrichtsTagCRUDRepository.deleteById(unterrichtstagId);
    }

    public void unterrichtstagUpdate(UnterrichtsTagDTO unterrichtsTagDTO, Integer unterrichtstagId) {
        UnterrichtsTag unterrichtstagdb = unterrichtsTagCRUDRepository.findById(unterrichtstagId).get();

        if (unterrichtsTagDTO.getTageEnums() != null)
            unterrichtstagdb.setTageEnums(unterrichtsTagDTO.getTageEnums());

        if (unterrichtsTagDTO.getFachId() != 0)
            unterrichtstagdb.getFach().unterrichtLoeschen(unterrichtstagdb);
        Fach fach = fachCRUDRepository.findById(unterrichtsTagDTO.getFachId()).get();
        unterrichtstagdb.setFach(fach);
        fach.setUnterichtsTag(unterrichtstagdb);
        fachCRUDRepository.save(fach);

        if (unterrichtsTagDTO.getTrainerId() != 0)
            unterrichtstagdb.getTrainerInnen().unterrichtLoeschen(unterrichtstagdb);
        TrainerInnen trainerInnen = trainerInnenCRUDRepository.findById(unterrichtsTagDTO.getTrainerId()).get();
        unterrichtstagdb.setTrainerInnen(trainerInnen);
        trainerInnen.setUnterichtsTag(unterrichtstagdb);
        trainerInnenCRUDRepository.save(trainerInnen);

        if (unterrichtsTagDTO.getGruppeId() != 0)
            unterrichtstagdb.getGruppe().unterrichtLoeschen(unterrichtstagdb);
        Gruppe gruppe = gruppeCRUDRepository.findById(unterrichtsTagDTO.getGruppeId()).get();
        unterrichtstagdb.setGruppe(gruppe);
        gruppe.setUnterichtsTag(unterrichtstagdb);
        gruppeCRUDRepository.save(gruppe);


        unterrichtsTagCRUDRepository.save(unterrichtstagdb);
    }
}
