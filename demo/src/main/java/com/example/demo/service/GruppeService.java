package com.example.demo.service;

import com.example.demo.dtos.GruppeDTO;
import com.example.demo.enums.GruppenEnums;
import com.example.demo.model.Gruppe;
import com.example.demo.model.Raum;
import com.example.demo.model.TeilnehmerInnen;
import com.example.demo.repositories.GruppeCRUDRepository;
import com.example.demo.repositories.RaumCRUDRepository;
import com.example.demo.repositories.TeilnehmerInnenCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GruppeService {

    @Autowired
    private GruppeCRUDRepository gruppeCRUDRepository;

    @Autowired
    private RaumCRUDRepository raumCRUDRepository;

    @Autowired
    private TeilnehmerInnenCRUDRepository teilnehmerInnenCRUDRepository;


    public List<Gruppe> getGruppe() {
        return (List<Gruppe>) gruppeCRUDRepository.findAll();
    }

    public Gruppe getGruppeById(Integer gruppenId) {
        return(Gruppe) gruppeCRUDRepository.findById(gruppenId).get();
    }


    public void neueGruppe(Gruppe gruppe) {
        gruppeCRUDRepository.save(gruppe);
    }

    public void setGruppe(int nummer, GruppenEnums gruppenEnums, Boolean qualifying, Integer raumId) {
        Raum raum = raumCRUDRepository.findById(raumId).get();

        Gruppe gruppe = new Gruppe(nummer, gruppenEnums, qualifying, raum);

        raum.setGruppe(gruppe);

        gruppeCRUDRepository.save(gruppe);
        raumCRUDRepository.save(raum);


    }

    public void gruppeLoeschen(Integer gruppeId) {
        boolean exists = gruppeCRUDRepository.existsById(gruppeId);
        if(!exists) {
            throw new IllegalStateException("Gruppe mit id " + gruppeId + " existiert nicht");
        }
        Gruppe gruppe = gruppeCRUDRepository.findById(gruppeId).get();

        if(gruppe.getRaum() != null) {
            Raum raum = gruppe.getRaum();
            raum.setGruppe(null);
            raumCRUDRepository.save(raum);
        }

        if(!gruppe.getTeilnehmerInnenSet().isEmpty() || gruppe.getTeilnehmerInnenSet() != null) {
            for(TeilnehmerInnen teilnehmerInnen : gruppe.getTeilnehmerInnenSet()) {
                teilnehmerInnen.setGruppe(null);
                teilnehmerInnenCRUDRepository.save(teilnehmerInnen);
            }
        }

        gruppeCRUDRepository.deleteById(gruppeId);
    }

    public void gruppeUpdate(GruppeDTO gruppeDTO, Integer gruppeId) {
        Gruppe gruppedb = gruppeCRUDRepository.findById(gruppeId).get();

        if (gruppeDTO.getNummer() != 0)
            gruppedb.setNummer(gruppeDTO.getNummer());
        if (gruppeDTO.getGruppenEnums() != null)
            gruppedb.setGruppenEnums(gruppeDTO.getGruppenEnums());

        if (gruppeDTO.getQualifying() == false || gruppeDTO.getQualifying() == true)
            gruppedb.setQualifying(gruppeDTO.getQualifying());

        if(gruppeDTO.getRaumId() == 0) {
            Raum raum = raumCRUDRepository.findById(gruppeDTO.getRaumId()).get();

            gruppedb.setRaum(raum);
            raum.setGruppe(gruppedb);

            raumCRUDRepository.save(raum);
        }

        if (gruppeDTO.getRaumId() != 0) {
            if (gruppedb.getRaum() != null)
                gruppedb.getRaum().gruppeLoeschen(gruppedb);
            Raum raum = raumCRUDRepository.findById(gruppeDTO.getRaumId()).get();

            gruppedb.setRaum(raum);
            raum.setGruppe(gruppedb);

            raumCRUDRepository.save(raum);


            gruppeCRUDRepository.save(gruppedb);
        }
    }
}
