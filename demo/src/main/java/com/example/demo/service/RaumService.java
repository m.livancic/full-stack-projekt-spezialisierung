package com.example.demo.service;

import com.example.demo.dtos.RaumDTO;
import com.example.demo.model.Gruppe;
import com.example.demo.model.Raum;
import com.example.demo.model.TeilnehmerInnen;
import com.example.demo.repositories.GruppeCRUDRepository;
import com.example.demo.repositories.RaumCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RaumService {

    @Autowired
    private RaumCRUDRepository raumCRUDRepository;

    @Autowired
    private GruppeCRUDRepository gruppeCRUDRepository;


    public List<Raum> getRaum() {
        return (List<Raum>) raumCRUDRepository.findAll();
    }

    public void neuerRaum(Raum raum) {
        raumCRUDRepository.save(raum);
    }

    public void setRaum(String name, int nummer) {

        Raum raum = new Raum (name, nummer);


        raumCRUDRepository.save(raum);

    }

    public void raumLoeschen(Integer raumId) {
        boolean exists = raumCRUDRepository.existsById(raumId);
        if(!exists) {
            throw new IllegalStateException("Raum mit id " + raumId + " existiert nicht");
        }
        Raum raum = raumCRUDRepository.findById(raumId).get();

        if(raum.getGruppe() != null) {
            Gruppe gruppe = raum.getGruppe();
            gruppe.setRaum(null);
            gruppeCRUDRepository.save(gruppe);
            raumCRUDRepository.save(raum);
        }



        raumCRUDRepository.deleteById(raumId);
    }

    public void raumUpdate(RaumDTO raumDTO, Integer raumId) {
        Raum raumdb = raumCRUDRepository.findById(raumId).get();

        if (raumDTO.getName() != null)
            raumdb.setName(raumDTO.getName());
        if (raumDTO.getNummer() != 0)
            raumdb.setNummer(raumDTO.getNummer());


            raumCRUDRepository.save(raumdb);
        }
    }

