package com.example.demo.service;

import com.example.demo.dtos.FachDTO;
import com.example.demo.enums.FachEnums;
import com.example.demo.model.Fach;
import com.example.demo.model.TeilnehmerInnen;
import com.example.demo.repositories.FachCRUDRepository;
import com.example.demo.repositories.UnterrichtsTagCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Clob;
import java.util.List;

@Service
public class FachService {

    @Autowired
    private FachCRUDRepository fachCRUDRepository;

    @Autowired
    private UnterrichtsTagCRUDRepository unterrichtsTagCRUDRepository;


    public FachService(FachCRUDRepository fachCRUDRepository) {
        this.fachCRUDRepository = fachCRUDRepository;
    }

    public List<Fach> getFach() {
        return (List<Fach>) fachCRUDRepository.findAll();
    }

    public void neuesFach(Fach fach) {
        fachCRUDRepository.save(fach);
    }

    public void setFach (String description, FachEnums fachEnums, String bild64) {

        Fach fach = new Fach(description, fachEnums, bild64);

        fachCRUDRepository.save(fach);
    }

    public void fachLoeschen(Integer fachId) {
        boolean exists = fachCRUDRepository.existsById(fachId);
        if(!exists) {
            throw new IllegalStateException("Fach mit id " + fachId + " existiert nicht");
        }



        fachCRUDRepository.deleteById(fachId);
    }

    public void fachUpdate(FachDTO fachDTO, Integer fachId) {
        Fach fachdb = fachCRUDRepository.findById(fachId).get();

        if (fachDTO.getDescription() != null)
            fachdb.setDescription(fachDTO.getDescription());
        if (fachDTO.getFachEnums() != null)
            fachdb.setFachEnums(fachDTO.getFachEnums());

        fachCRUDRepository.save(fachdb);
    }

}
