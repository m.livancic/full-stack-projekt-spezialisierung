package com.example.demo.service;

import com.example.demo.dtos.TrainerInnenDTO;
import com.example.demo.enums.AnstellungEnums;
import com.example.demo.model.Fach;
import com.example.demo.model.TeilnehmerInnen;
import com.example.demo.model.TrainerInnen;
import com.example.demo.repositories.FachCRUDRepository;
import com.example.demo.repositories.TrainerInnenCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrainerInnenService {

    @Autowired
    private TrainerInnenCRUDRepository trainerInnenCRUDRepository;

    @Autowired
    private FachCRUDRepository fachCRUDRepository;


    public List<TrainerInnen> getTrainerInnen(){
        return (List<TrainerInnen>) trainerInnenCRUDRepository.findAll();
    }

    public TrainerInnen getTrainerInnenbyId(Integer trainerInnenId) {
        return (TrainerInnen) trainerInnenCRUDRepository.findById(trainerInnenId).get();
    }

    public void setTrainnerinnen(String name, String eMail, String adresse, String telnum, AnstellungEnums anstellungEnums, float bruttoGehalt, Integer fachId) {
        Fach fach = fachCRUDRepository.findById(fachId).get();

        TrainerInnen trainerInnen = new TrainerInnen(name, eMail, adresse, telnum, anstellungEnums, bruttoGehalt, fach);

        fach.setTrainnerInnen(trainerInnen);

        trainerInnenCRUDRepository.save(trainerInnen);
        fachCRUDRepository.save(fach);
    }

    public void neueTrainerIn (TrainerInnen trainerInnen) {
        trainerInnenCRUDRepository.save(trainerInnen);
    }

    public void trainerInloeschen (Integer trainerInnenId) {
        boolean exists = trainerInnenCRUDRepository.existsById(trainerInnenId);
        if(!exists) {
            throw new IllegalStateException("TrainerIn mit id " + trainerInnenId + " existiert nicht");
        }//trainerInnenCRUDRepository.deleteById(trainerInnenId);
        trainerInnenCRUDRepository.deleteById(trainerInnenId);

    }

    public void trainerInUpdate(TrainerInnenDTO trainerInnenDTO, Integer trainerInnenId) {
        TrainerInnen trainerInnendb = trainerInnenCRUDRepository.findById(trainerInnenId).get();


        if (trainerInnenDTO.getName() != null)
            trainerInnendb.setName(trainerInnenDTO.getName());
        if (trainerInnenDTO.getAdresse() != null)
            trainerInnendb.setAdresse(trainerInnenDTO.getAdresse());
        if (trainerInnenDTO.geteMail() != null)
            trainerInnendb.seteMail(trainerInnenDTO.geteMail());
        if (trainerInnenDTO.getTelnum() != null)
            trainerInnendb.setTelnum(trainerInnenDTO.getTelnum());
        if (trainerInnenDTO.getAnstellungEnums() != null)
            trainerInnendb.setAnstellungsart(trainerInnenDTO.getAnstellungEnums());
        if (trainerInnenDTO.getBruttoGehalt() != 0)
            trainerInnendb.setBruttoGehalt(trainerInnenDTO.getBruttoGehalt());
        if (trainerInnenDTO.getFachId() != 0)
            trainerInnendb.getFach().fachLoeschen(trainerInnendb);
        Fach fach = fachCRUDRepository.findById(trainerInnenDTO.getFachId()).get();
        trainerInnendb.setFach(fach);
        fach.setTrainnerInnen(trainerInnendb);
        fachCRUDRepository.save(fach);



        trainerInnenCRUDRepository.save(trainerInnendb);
    }


    public double nettoGehalt(int trainerId) {
        TrainerInnen trainerInnen = trainerInnenCRUDRepository.findById(trainerId).get();

        double nettoSallary = trainerInnen.getBruttoGehalt() * 0.8;

        return nettoSallary;
    }


}
